package ch.unige.gitlab.openweatherappkotlin

import WeatherService
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val entreeLocation = findViewById<EditText>(R.id.entreeLocation)
        val bouton = findViewById<Button>(R.id.button)

        getWeather("Genève")

        bouton.setOnClickListener {
            getWeather(entreeLocation.text.toString())
            hideKeyboard()
        }

    }

    fun getWeather(ville: String) {

        val temperature = findViewById<TextView>(R.id.temperature)
        val location = findViewById<TextView>(R.id.location)
        val conditionMeteo = findViewById<TextView>(R.id.weather_condition)

        val humidity = findViewById<TextView>(R.id.humidityVal)
        val ressenti = findViewById<TextView>(R.id.ressentieVal)
        val vent = findViewById<TextView>(R.id.ventVal)
        val visibility = findViewById<TextView>(R.id.visibilityVal)
        val minMax = findViewById<TextView>(R.id.minMaxVal)
        val pression = findViewById<TextView>(R.id.pressionVal)

        val weatherIcon = findViewById<ImageView>(R.id.weatherIcon)


        val request = ServiceBuilder.buildService(Endpoints::class.java)
        val call = request.getWeather(
            appid = "4a4ff120d580d77472690b4b3e54ec28",
            location = ville,
            lang = "fr",
            units = "metric"
        )
        call.enqueue(object : Callback<WeatherService> {
            override fun onResponse(
                call: Call<WeatherService>,
                response: Response<WeatherService>
            ) {
                if (response.isSuccessful) {


                    Picasso.with(applicationContext)
                        .load("https://openweathermap.org/img/wn/" + response.body()!!.weather[0].icon + "@4x.png").into(weatherIcon)

                    temperature.text = response.body()!!.main.temp.toInt().toString()
                    location.text = response.body()!!.name + ", " + response.body()!!.sys.country
                    conditionMeteo.text = response.body()!!.weather[0].description


                    humidity.text = response.body()!!.main.humidity.toString() + " %"
                    ressenti.text = response.body()!!.main.feelsLike.toInt().toString() + "°"
                    vent.text = (response.body()!!.wind.speed * 3.6).toInt().toString() + " Km/h"
                    visibility.text = response.body()!!.visibility.toString() + " m"
                    minMax.text = response.body()!!.main.tempMax.toInt().toString() + "° / " + response.body()!!.main.tempMin.toInt().toString() + "°"
                    pression.text = response.body()!!.main.pressure.toString() + " hPa"

                }
            }

            override fun onFailure(call: Call<WeatherService>, t: Throwable) {
                Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_SHORT).show()
            }
        })

    }


    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}