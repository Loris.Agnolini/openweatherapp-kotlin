package ch.unige.gitlab.openweatherappkotlin

import WeatherService
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Endpoints {
    //représente l'appel réseau (endpoint) que nous allons réaliser sur l'API
    @GET("/data/2.5/weather?")
    fun getWeather(
        @Query("q") location: String,  //location
        @Query("units") units: String, //unités
        @Query("lang") lang: String,   //langue
        @Query("appid") appid: String  //clé API
    ): Call<WeatherService>

}